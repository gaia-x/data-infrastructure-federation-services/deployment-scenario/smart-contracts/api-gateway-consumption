
const path = require('path');
const Minio = require('minio');
const axios = require('axios');
const xml2js = require('xml2js');
const fs = require('fs').promises;
const logger = require('../services/logger');
const HttpError = require('../utils/httpError');

function getMinioClientWithCredentials() {
    try {
        const minioClient = new Minio.Client({
            endPoint: process.env.MINIO_ENDPOINT,
            port: parseInt(process.env.MINIO_PORT),
            useSSL: true,
            accessKey: process.env.MINIO_ACCESS_KEY,
            secretKey: process.env.MINIO_SECRET_KEY
        });
    
        return minioClient;
    }
    catch (error) {
        throw(error instanceof HttpError ? error : new HttpError(error.message, 500));
    }
}


async function getMinioClientWithKeycloakToken(jwt) {
    try {
        const response = await axios.post('http://127.0.0.1:9000', null, {
            params: {
                Action: 'AssumeRoleWithWebIdentity',
                Version: '2011-06-15',
                DurationSeconds: '86000',
                WebIdentityToken: jwt
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });

        const result = await xml2js.parseStringPromise(response.data);
        const credentials = result.AssumeRoleWithWebIdentityResponse.AssumeRoleWithWebIdentityResult[0].Credentials[0];
        const accessKey = credentials.AccessKeyId[0];
        const secretKey = credentials.SecretAccessKey[0];
        const sessionToken = credentials.SessionToken[0];

        const minioClient = new Minio.Client({
            endPoint: process.env.MINIO_ENDPOINT,
            port: parseInt(process.env.MINIO_PORT),
            useSSL: false,
            accessKey: accessKey,
            secretKey: secretKey,
            sessionToken: sessionToken
        });

        return minioClient;
    }
    catch (error) {
        throw(error instanceof HttpError ? error : new HttpError(error.message, 500));
    }
}


async function listBucketsName(minioClient) {
    try {
        const buckets = await minioClient.listBuckets();
        logger.info('Buckets: ', buckets);
        return buckets.map((item) => item.name);
    }
    catch (error) {
        throw(error instanceof HttpError ? error : new HttpError(error.message, 500));
    }
}

async function listAllDatasetsDetails() {
    const minioClient = getMinioClientWithCredentials();
    const bucketNames = await listBucketsName(minioClient);

    const datasetDetails = {};
    for (const bucketName of bucketNames) {
        const fileNames = [];

        try {
            const stream = await minioClient.listObjects(bucketName, '', true);
            for await (const obj of stream) {
                fileNames.push(obj.name);
            }
        } catch (e) {
            throw(error instanceof HttpError ? error : new HttpError(`Failed to read file name from bucket: ${error.message}`, 500));
        }
        datasetDetails[bucketName] = fileNames;
    }

    return datasetDetails;
}


async function getAllDataElementsInABucket(bucketName) {
    const minioClient = getMinioClientWithCredentials();
    const datasetsDetails = await listAllDatasetsDetails(minioClient);

    if (!datasetsDetails[bucketName]) {
        throw new HttpError(`Bucket ${bucketName} does not exist, not authorized`, 401);
    }

    const filesInBucket = datasetsDetails[bucketName];
    const resources = [];

    for (const file of filesInBucket) {
        try {
            resources.push({ filename: file, url: `${process.env.BASE_URL}/datasets/${bucketName}/${file}` });
        } catch (error) {
            throw(error instanceof HttpError ? error : new HttpError(error.message, 500));
        }
    }

    return resources;
}

async function getFileSize(bucketName, fileName) {
    try {
        const minioClient = getMinioClientWithCredentials();
        const stat = await minioClient.statObject(bucketName, fileName);
        logger.info(`File size for ${bucketName}/${fileName}: ${stat.size}`);
        return stat.size;
    } catch (error) {
        throw(error instanceof HttpError ? error : new HttpError(error.message, 500));
    }
}


async function downloadFileInBucket(dataset, fileName) {
    const filePath = await getDataElementInABucket(dataset, fileName);

    try {
        await fs.access(filePath, fs.constants.F_OK);
        const contentType = getContentType(fileName);
        return { filePath, contentType };
    } catch (error) {
        throw new HttpError(`File ${fileName} does not exist`, 404);
    }
}

async function getDataElementInABucket(dataset, filename) {
    const minioClient = getMinioClientWithCredentials();

    try {
        await minioClient.fGetObject(dataset, filename, `data/${dataset}/${filename}`);
        return path.join(__dirname, '../../data', dataset, filename);
    } catch (e) {
        throw new HttpError(`Failed to read document with name ${filename} from Minio`, 500);
    }
}

function getContentType(fileName) {
    const extension = path.extname(fileName).toLowerCase();
    switch (extension) {
        case '.pdf':
            return 'application/pdf';
        case '.txt':
            return 'text/plain';
        case '.jpg':
        case '.jpeg':
            return 'image/jpeg';
        default:
            return 'application/octet-stream';
    }
}


module.exports = {
    getFileSize,
    downloadFileInBucket,
    listAllDatasetsDetails,
    getAllDataElementsInABucket,
    getMinioClientWithCredentials,
    getMinioClientWithKeycloakToken
}