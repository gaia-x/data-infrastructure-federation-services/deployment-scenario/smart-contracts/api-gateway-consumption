const path = require('path');
const axios = require('axios');
const HttpError = require('../utils/httpError');


async function sendDataToBeTraced(ipAddress, fileSize, fileName) {
    try {
        const data = {
            date: new Date().toISOString(),
            ipAddress: ipAddress,
            fileName: fileName,
            fileSize: fileSize.toString(),
            fileType: (path.extname(fileName)).slice(1)
        }

        console.log(data);

        await axios.post(`${process.env.SMART_CONTRACT_MANAGER_URL}/blockchain/dataComsumption`, data, {
            headers: {
                'x-api-key': process.env.SMART_CONTRACT_MANAGER_API_KEY,
                'Content-Type': 'application/json'
            }
        });
    }
    catch (error) {
        throw new HttpError(`Error to send data to be traced to the smart contract manager: ${error.message}`, 500);
    }
}



module.exports = {
    sendDataToBeTraced
}