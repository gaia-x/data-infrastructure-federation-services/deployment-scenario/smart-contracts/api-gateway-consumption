const cors = require('cors');
const express = require('express');
const logger = require('./services/logger');
const HttpError = require('./utils/httpError');
const minioController = require('./controllers/minioController');

/* For local development */
if (process.env.NODE_ENV !== "production") require("dotenv").config();

/* Express Configuration */
const PORT = 8085;
const app = express();
app.use(cors());
app.use((req, res, next) => {
    logger.info(`Received request on ${req.method} ${req.path}`);
    next();
});


/* Minio Routes */
app.get('/api/v1/datasets', minioController.getAllDatasets);
app.get('/api/v1/datasets/:dataset', minioController.getAllFilesInDataset);
app.get('/datasets/:dataset/:filename', minioController.downloafFileInDataset);



/* Error */
app.use((error, req, res, next) => {
    logger.error(error.message, { stackTrace: error.stack ? error.stack : 'No stack trace' });
    if (error instanceof HttpError && error.statusCode !== 500) {
        res.status(error.statusCode).json({ error: error.message });
    } else {
        res.status(500).json({ error: 'Internal server error' });
    }
});

/* Start the server */
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});