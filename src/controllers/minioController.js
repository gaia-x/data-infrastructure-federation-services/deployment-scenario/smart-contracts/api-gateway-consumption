const HttpError = require('../utils/httpError');
const minioService = require('../services/minio');
const smartContractManagerService = require('../services/smartContractManager');


exports.getAllDatasets = async (req, res, next) => {
    try {
        // MinioClient with keycloak, but config dont work
        // const authorizationToken = req.headers['authorization'];
        // if (!authorizationToken) {
        //     return res.status(401).send('Authorization token is missing');
        // }
        // const jwtToken = authorizationToken.replace("Bearer ", "");
        // const minioClientByKeycloak = await minioService.getMinioClientWithKeycloakToken(jwtToken);

        const allDatasets = await minioService.listAllDatasetsDetails();
        res.status(200).json(allDatasets);
    }
    catch (error) {
        next(error instanceof HttpError ? error : new HttpError(error.message, 500));
    }
};

exports.getAllFilesInDataset = async (req, res, next) => {
    try {
        // MinioClient with keycloak, but config dont work
        // const authorizationToken = req.headers['authorization'];
        // if (!authorizationToken) {
        //     return res.status(401).send('Authorization token is missing');
        // }
        // const jwtToken = authorizationToken.replace("Bearer ", "");
        // const minioClientByKeycloak = await minioService.getMinioClientWithKeycloakToken(jwtToken);

        const respone = await minioService.getAllDataElementsInABucket(req.params.dataset);
        res.status(200).json(respone);
    }
    catch (error) {
        next(error instanceof HttpError ? error : new HttpError(error.message, 500));
    }
};


exports.downloafFileInDataset = async (req, res, next) => {
    try {
        // MinioClient with keycloak, but config dont work
        // const authorizationToken = req.headers['authorization'];
        // if (!authorizationToken) {
        //     return res.status(401).send('Authorization token is missing');
        // }
        // const jwtToken = authorizationToken.replace("Bearer ", "");
        // const minioClientByKeycloak = await minioService.getMinioClientWithKeycloakToken(jwtToken); // For check the authorization

        /* Send data to be traced to the smart contract manager */
        const fileSize = await minioService.getFileSize(req.params.dataset, req.params.filename);
        //await smartContractManagerService.sendDataToBeTraced(req.ip, fileSize, req.params.filename);

        /* Download file from minio */
        const { filePath, contentType } = await minioService.downloadFileInBucket(req.params.dataset, req.params.filename);
        res.setHeader('Content-Type', contentType);
        res.download(filePath, req.params.filename, (err) => {
            if (err) {
                throw new HttpError(`Error while downloading file: ${err.message}`, 500);
            }
        });
    }
    catch (error) {
        next(error instanceof HttpError ? error : new HttpError(error.message, 500));
    }
};